// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Example.iOS.Indoor
{
    [Register ("ViewController2")]
    partial class ViewController2
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel label1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel label11 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel label2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel label3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel label4 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel label5 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel label6 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel label7 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel label8 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewController { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (label1 != null) {
                label1.Dispose ();
                label1 = null;
            }

            if (label11 != null) {
                label11.Dispose ();
                label11 = null;
            }

            if (label2 != null) {
                label2.Dispose ();
                label2 = null;
            }

            if (label3 != null) {
                label3.Dispose ();
                label3 = null;
            }

            if (label4 != null) {
                label4.Dispose ();
                label4 = null;
            }

            if (label5 != null) {
                label5.Dispose ();
                label5 = null;
            }

            if (label6 != null) {
                label6.Dispose ();
                label6 = null;
            }

            if (label7 != null) {
                label7.Dispose ();
                label7 = null;
            }

            if (label8 != null) {
                label8.Dispose ();
                label8 = null;
            }

            if (ViewController != null) {
                ViewController.Dispose ();
                ViewController = null;
            }
        }
    }
}