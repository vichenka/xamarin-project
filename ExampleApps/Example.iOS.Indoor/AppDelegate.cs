﻿using Foundation;
using UIKit;

using System.Diagnostics;

using Estimote.iOS.Indoor;
using CoreLocation;

namespace Example.iOS.Indoor
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations

        public override UIWindow Window
        {
            get;
            set;
        }


        private CLLocationManager _locationManager;
        private CLBeaconRegion _region;

        EILIndoorLocationManager locationManager;

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            //Window = new UIWindow(UIScreen.MainScreen.Bounds);
          
            //var controller = new UIViewController();           
            //var navController = new ViewController1(controller);
           //label1 = new UILabel () { Text = "This is a label 1.", TextColor = UIColor.Magenta};  
           // var cvc = new ViewController2();
           // var navController = new UINavigationController(cvc);
           // navController.View.BackgroundColor = UIColor.LightGray;
           // Window.RootViewController = navController;
            // make the window visible
           // Window.MakeKeyAndVisible();

          

            var userNotificationSettings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert, new NSSet());
            UIApplication.SharedApplication.RegisterUserNotificationSettings(userNotificationSettings);

            _locationManager = new CLLocationManager();
            _locationManager.AuthorizationChanged += LocationManagerAuthorizationChanged;
            _locationManager.RegionEntered += LocationManagerRegionEntered;
            _locationManager.RegionLeft += LocationManagerRegionLeft;
            _locationManager.RequestAlwaysAuthorization();            

          //  return true;
            // get your app ID and token on:
            // https://cloud.estimote.com/#/apps/add/your-own-app
            ESTConfig.SetupAppIdAndToken("estimote-xamarin-6a7", "f67de2cae86b46a01831edfba4248839");

            locationManager = new EILIndoorLocationManager();

            // C# style delegates, i.e., events & event handlers
            locationManager.DidUpdatePosition += (sender, e) =>
            {
                var notification = new UILocalNotification();
                notification.FireDate = NSDate.FromTimeIntervalSinceNow(10);
                notification.AlertBody="DidUpdatePosition:"  + e.Position.X + e.Position.Y;
                UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
            };
            locationManager.DidFailToUpdatePosition += (sender, e) =>
            {
                var notification = new UILocalNotification();
                notification.AlertBody= "DidFailToUpdatePosition:";
                UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
            };
 
           
            // For Obj-C style delegates, you'd want to have a class that derives from EILIndoorLocationManagerDelegate, implements the DidUpdatePosition and DidFailToUpdatePosition methods, and do:
            // locationManager.Delegate = myDelegateObject;
            //
            // Or, implement the DidUpdatePosition and DidFailToUpdatePosition methods on any class, and do:
            // locationManager.WeakDelegate = myDelegateObject;
            //
            // The latter is helpful in some cases, because Xamarin Delegates are implemented as classes, not interfaces, and thus are subject to the "single-class inheritance" rule.
            // 
            // For example, you can't have AppDelegate be both UIApplicationDelegate and EILIndoorLocationManagerDelegate.
            // But you can implement the EILIndoorLocationManagerDelegate methods in your AppDelegate, and assign it as a WeakDelegate.

            Debug.WriteLine("Fetching location…");
            new EILRequestFetchLocation("my-test-location").SendRequest((location, error) =>
            {
                if (location != null)
                {
                    Debug.WriteLine("Fetching location success! Starting position updates");
                    locationManager.StartPositionUpdatesForLocation(location);
                }
                else
                {
                    Debug.WriteLine($"Can't fetch location: {error}");
                }
            });

            return true;
        }

        void LocationManagerAuthorizationChanged(object sender, CLAuthorizationChangedEventArgs e)
        {
            Debug.WriteLine("Authorisation state: {0}", e.Status);

            if (e.Status == CLAuthorizationStatus.AuthorizedAlways)
            {
                _region = new CLBeaconRegion(new NSUuid("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), "My region");
                _locationManager.StartMonitoring(_region);
            }
        }

        void LocationManagerRegionLeft(object sender, CLRegionEventArgs e)
        {
            var notification = new UILocalNotification();
            notification.FireDate = NSDate.FromTimeIntervalSinceNow(10);
            notification.AlertBody = "Goodbye iBeacon!";
            UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
        }

        void LocationManagerRegionEntered(object sender, CLRegionEventArgs e)
        {
            var notification = new UILocalNotification();
            notification.AlertBody = "Hello iBeacon!";
            UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
        }               

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }
    }
}
