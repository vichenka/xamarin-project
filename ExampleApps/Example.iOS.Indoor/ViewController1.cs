
using System;
using System.Drawing;
using CoreLocation;
using Foundation;
using UIKit;

namespace Example.iOS.Indoor
{
    public partial class ViewController1 : UIViewController
    {

        NSUuid UUID = new NSUuid("B9407F30-F5F8-466E-AFF9-25556B57FE6D");
        ushort major = 9352;
        string regionIdentifier = "DemoRegion";
        CLLocationManager locationMgr;
        UILabel label2 = new UILabel() { Text = "This is a label 2.", TextColor = UIColor.Magenta };
        UILabel label = new UILabel();

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.BackgroundColor = UIColor.Gray;
            var beaconRegion = new CLBeaconRegion(UUID, major, regionIdentifier);
            beaconRegion.NotifyEntryStateOnDisplay = true;

            locationMgr = new CLLocationManager();
            locationMgr.DidRangeBeacons += (object sender, CLRegionBeaconsRangedEventArgs e) =>
            {
                if (e.Beacons.Length > 0)
                {
                    var beacon = e.Beacons[0];
                   
              
                    label.Text = "Streeght: " + beacon.Rssi + "Distanse: " + beacon.Accuracy.ToString();

                }
            
            };
            locationMgr.StartMonitoring(beaconRegion);
            locationMgr.StartRangingBeacons(beaconRegion);
        }

        public ViewController1(IntPtr handle) : base(handle)
        {
        }

        public ViewController1() { }

        public ViewController1(UIViewController controller)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        //public override void ViewDidLoad()
        //{
        //    base.ViewDidLoad();

        //    // Perform any additional setup after loading the view, typically from a nib.
        //}

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}
