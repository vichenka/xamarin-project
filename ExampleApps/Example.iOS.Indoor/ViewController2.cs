using CoreLocation;
using Foundation;
using System;
using System.Linq;
using UIKit;

namespace Example.iOS.Indoor
{
    public partial class ViewController2 : UIViewController
    {
        public ViewController2()
        {
        }

        public ViewController2(IntPtr handle) : base(handle)
        {

        }
        ////NSUuid UUID1 = new NSUuid("B9407F30-F5F8-466E-AFF9-25556B57FE6D");
        ////ushort major1 = 9352;
        //NSUuid UUID1 = new NSUuid("B9407F30-F5F8-466E-AFF9-25556B57FE6D");
        //ushort major1 = 57079;
        //string regionIdentifier = "DemoRegion";
        //CLLocationManager locationMgr1;

        //NSUuid UUID2 = new NSUuid("B9407F30-F5F8-466E-AFF9-25556B57FE6D");
        //ushort major2 = 31846;
        //string regionIdentifier2 = "DemoRegion";
        //CLLocationManager locationMgr2;

        //public override void ViewDidLoad()
        //{
        //    base.ViewDidLoad();

        //    var beaconRegion1 = new CLBeaconRegion(UUID1, major1, regionIdentifier);
        //    beaconRegion1.NotifyEntryStateOnDisplay = true;
        //    locationMgr1 = new CLLocationManager();

        //    var beaconRegion2 = new CLBeaconRegion(UUID2, major2, regionIdentifier2);
        //    beaconRegion2.NotifyEntryStateOnDisplay = true;
        //    locationMgr2 = new CLLocationManager();

        //    locationMgr1.DidRangeBeacons += (object sender, CLRegionBeaconsRangedEventArgs e) =>
        //    {
        //        label1.Text = "label1";
        //        label2.Text = "label2";
        //        label3.Text = "label3";
        //        label4.Text = "label4";
        //        label5.Text = "label5";
        //        label6.Text = "label6";
        //        label7.Text = "label7";
        //        // label8.Text = "label8";

        //        if (e.Beacons.Length > 0)
        //        {
        //            var beacon = e.Beacons[0];

        //           // label1.Text = beacon.Rssi.ToString();
        //            label1.Text = "1" + beacon.Accuracy.ToString();
        //            label2.Text ="1" +  beacon.Proximity.ToString();

        //            //  label1.Text = "Streeght: " + beacon.Rssi + "Distanse: " + beacon.Accuracy.ToString();

        //        }

        //    };

        //    locationMgr2.DidRangeBeacons += (object sender, CLRegionBeaconsRangedEventArgs e) =>
        //    {

        //        if (e.Beacons.Length > 0)
        //        {
        //            var beacon = e.Beacons[0];

        //            label3.Text = "2" + beacon.Accuracy.ToString();
        //            label4.Text = "2" + beacon.Proximity.ToString();                    
        //        }
        //    };
        //    locationMgr2.StartMonitoring(beaconRegion2);
        //    locationMgr2.StartRangingBeacons(beaconRegion2);
        //    locationMgr1.StartMonitoring(beaconRegion1);
        //    locationMgr1.StartRangingBeacons(beaconRegion1);
        //}
        private CLLocationManager _locationManager;
        private CLBeaconRegion _region1;
        private CLBeaconRegion _region2;
        private CLBeaconRegion _region3;
        private CLBeaconRegion _region4;

        private double _beacon1Rssi;
        private double _beacon2Rssi;
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            _locationManager = new CLLocationManager();
            _locationManager.AuthorizationChanged += LocationManagerAuthorizationChanged;
            _locationManager.DidRangeBeacons += LocationManagerDidRangeBeacons;

            _locationManager.RequestAlwaysAuthorization();

            _region1 = new CLBeaconRegion(new NSUuid("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), 9352, 22637, "Beacon1");
            _region2 = new CLBeaconRegion(new NSUuid("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), 31846, 37559, "Beacon2");
            _region3 = new CLBeaconRegion(new NSUuid("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), 57079, 22865, "Beacon3");
            _region4 = new CLBeaconRegion(new NSUuid("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), 34428, 35572, "Beacon4");
        }

        void LocationManagerAuthorizationChanged(object sender, CLAuthorizationChangedEventArgs e)
        {
            _locationManager.StartMonitoring(_region1);
            _locationManager.StartMonitoring(_region2);
            _locationManager.StartMonitoring(_region3);
            _locationManager.StartMonitoring(_region4);

            if (e.Status == CLAuthorizationStatus.AuthorizedAlways)
            {
                _locationManager.StartRangingBeacons(_region1);
                _locationManager.StartRangingBeacons(_region2);
                _locationManager.StartRangingBeacons(_region3);
                _locationManager.StartRangingBeacons(_region4);
            }
        }

        void LocationManagerDidRangeBeacons(object sender, CLRegionBeaconsRangedEventArgs e)
        {
            //.Where(b => b.Rssi != 0)
            CLBeacon nearest = null;
            foreach (var beacon in e.Beacons)
            {
                bool isFirst = true;
                if (isFirst)
                    label1.Text = beacon.Proximity.ToString() + "\n";
                else
                    label1.Text += beacon.Proximity.ToString() + "\n";
                isFirst = false;
                //int ind = 1;
                //switch (ind)
                //{
                //    case 1:
                //        label1.Text = beacon.Proximity.ToString();
                //        break;
                //    case 2:
                //        label2.Text = beacon.Proximity.ToString();
                //        break;
                //}
                //ind++;
                //label1.Text = beacon.ProximityUuid.AsString();
                //label2.Text = beacon.Accuracy.ToString();
                //label3.Text = beacon.Proximity.ToString();


                if (nearest == null || nearest.Rssi < beacon.Rssi)
                    nearest = beacon;
            }

            if (nearest == null)
                label1.Text = "No beacons found";
           // else
            //    label1.Text = $"Nearest Beacon is {nearest.Major}.{nearest.Minor}";
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }

}
